# soc-ressources

Docs et ressources

## Objectifs

Créer une liste de ressources intéressante autour des problématiques de sécurité

## Sites Insitutionnels
- [ANSSI Bonnes Pratiques](https://www.ssi.gouv.fr/entreprise/bonnes-pratiques/)
- [Cert-FR](https://www.cert.ssi.gouv.fr/)
- [canadian Cybercenter](https://cyber.gc.ca/en/guidance/top-10-it-security-actions-protect-internet-connected-networks-and-information-itsm10089)

## Bookmarks

- [ ] [decalage](https://github.com/decalage2/awesome-security-hardening#windows)

liste de checklist et ressources de sécu

## Outils
- [sysmon](https://docs.microsoft.com/en-us/sysinternals/downloads/sysmon)
- [osquery](https://osquery.io)
- [hardening kitty](https://github.com/scipag/HardeningKitty)

## Windows
- Point de controles AD [Cert-FR](https://www.cert.ssi.gouv.fr/uploads/guide-ad.html)
- Durcissement [Cert-FR](https://www.cert.ssi.gouv.fr/dur/CERTFR-2020-DUR-001/)
- sessions actives : [logonsessions](https://learn.microsoft.com/fr-fr/sysinternals/downloads/logonsessions)
- utilisateurs loggues : [psloggedon](https://learn.microsoft.com/fr-fr/sysinternals/downloads/psloggedon)
- net session
- rootkit : [rootkit revealer M$](https://learn.microsoft.com/fr-fr/sysinternals/downloads/rootkit-revealer)
- redline host investigation : [redline](https://www.fireeye.com/services/freeware/redline.html)

## Hashages

verifier un hash de fichier : 

certutil -hashfile sysmon-config.xml SHA1
certutil -hashfile sysmon-config.xml MD5

## journalisation M$
- guide de journalisation ANSSI : [guide journalisation microsoft](https://github.com/ANSSI-FR/guide-journalisation-microsoft)