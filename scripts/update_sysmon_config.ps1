# install and config Path
$installPath = 'c:\sources'
mkdir -p $installPath

$configPath = 'c:\defaults'
mkdir -p $configPath

# download base config
#$uriConf = 'https://raw.githubusercontent.com/SwiftOnSecurity/sysmon-config/1836897f12fbd6a0a473665ef6abc34a6b497e31/sysmonconfig-export.xml'
#$uriConf = 'https://gitlab.com/jiiro974/ressources/-/raw/main/conf/sysmon-config.xml'
$uriConf = 'https://gitlab.com/jiiro974/ressources/-/raw/main/conf/sysmon-modular/sysmonconfig.xml'
$destConf = $configPath+'\sysmon-config.xml'
Invoke-RestMethod -Uri $uriConf -OutFile $destConf

# install service + config
cd $installPath\sysmon

# # in doubt force re install
# Start-Process -Verb RunAs .\Sysmon64.exe -Args {"-accepteula"; "-i $destConf"}

# update sysmon config
Start-Process -Verb RunAs .\Sysmon64.exe -Args "-c $destConf"
